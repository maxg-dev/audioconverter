import os

from setuptools import setup, find_packages


def get_version():
    with open('VERSION', 'r') as f:
        return f.read().strip()


def get_long_description():
    with open('README.md', 'r') as f:
        return f.read().strip()

def get_requirements():
    with open('requirements.txt', 'r') as f:
        return f.read().splitlines()


setup(
    name='audioconverter',
    fullname='AudioConverter',
    version=get_version(),
    description='A batch audio converter powered by FFmpeg.',
    long_description=get_long_description(),
    keywords='FFmpeg audio converter encoder music',
    author='Max G',
    author_email='max@maxg.dev',
    license='GPLv3',
    python_requires='>=3.6',
    packages=find_packages(),
    include_package_data=True,
    entry_points={
        'console_scripts': [
            'audiocon=audioconverter.cli:main',
        ]
    },
    install_requires=get_requirements(),
)
