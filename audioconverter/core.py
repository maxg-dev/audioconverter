import abc
import os
import re
import shutil
import sys
import threading
import time
import timeit

from audioconverter import log
from enum import Enum
from subprocess import Popen, PIPE

DEFAULTPATH = os.path.join(os.path.expanduser("~"), "Music", "AudioConverter")


class CompType(Enum):
    LOSSY = 1
    LOSSLESS = 2


def codecfactory(codec):
    codec = codec.lower()
    if codec == "aac":
        return AAC()
    elif codec == "alac":
        return ALAC()
    elif codec == "flac":
        return FLAC()
    elif codec == "mp3":
        return MP3()
    elif codec == "mp3_cbr":
        return MP3(cbr=True)
    elif codec == "opus":
        return OPUS()
    elif codec == "vorbis":
        return Vorbis()
    elif codec == "wav":
        return WAV()
    else:
        raise AssertionError(f"{codec} is not a supported codec.")


class CODEC(abc.ABC):
    def __init__(self, name, ext, binary=None):
        self.name = name
        self.ext = ext
        if not binary:
            binary = "ffmpeg"
        self.binary = binary

    def __str__(self):
        return f"name: {self.name}"

    @abc.abstractmethod
    def getargs(self, ifile, ofile):
        pass

    def encode(self, ifile, ofile):
        result = {"stout": "", "sterr": "", "returncode": None}
        p = Popen(
            self.getargs(ifile, ofile), stdout=PIPE, stderr=PIPE, encoding="utf-8"
        )
        result["stout"], result["sterr"] = p.communicate()
        result["returncode"] = p.returncode
        return result


class LosslessCodec(CODEC):
    def __init__(self, name, ext, binary=None, compvals=None):
        super().__init__(name, ext, binary)
        self.comptype = CompType.LOSSLESS


class LossyCodec(CODEC):
    def __init__(self, name, ext, quality, binary=None, compvals=None):
        super().__init__(name, ext, binary)
        self.comptype = CompType.LOSSY
        if not compvals:
            compvals = [
                "320k",
                "256k",
                "224k",
                "192k",
                "160k",
                "128k",
                "112k",
                "96k",
                "80k",
            ]
        self.compvals = compvals
        self.quality = quality

    @property
    def quality(self):
        return self._quality

    @quality.setter
    def quality(self, quality):
        quality = int(quality)
        if not (quality >= 0 and quality <= len(self.compvals) - 1):
            raise ValueError(f"{quality} is not a valid value for quality.")
        self._quality = quality

    def __str__(self):
        return super().__str__() + f", quality: {self.quality}"

    @property
    def compval(self):
        return self.compvals[self.quality]


class AAC(LossyCodec):
    def __init__(self, quality=2, binary=None):
        super().__init__(
            "aac",
            ".m4a",
            quality,
            binary,
            compvals=[
                "256k",
                "224k",
                "192k",
                "160k",
                "128k",
                "112k",
                "96k",
                "80k",
                "64k",
            ],
        )

    def getargs(self, ifile, ofile):
        return [
            self.binary,
            "-hide_banner",
            "-v",
            "error",
            "-i",
            ifile,
            "-c:a",
            self.name,
            "-b:a",
            self.compval,
            "-vn",
            "-y",
            ofile,
        ]


class ALAC(LosslessCodec):
    def __init__(self, binary=None):
        super().__init__("alac", ".m4a", binary)

    def getargs(self, ifile, ofile):
        return [
            self.binary,
            "-hide_banner",
            "-v",
            "error",
            "-i",
            ifile,
            "-c:a",
            self.name,
            "-vn",
            "-y",
            ofile,
        ]


class FLAC(LosslessCodec):
    def __init__(self, binary=None):
        super().__init__("flac", ".flac", binary)

    def getargs(self, ifile, ofile):
        return [
            self.binary,
            "-hide_banner",
            "-v",
            "error",
            "-i",
            ifile,
            "-c:a",
            self.name,
            "-vn",
            "-y",
            ofile,
        ]


class MP3(LossyCodec):
    def __init__(self, quality=1, binary=None, cbr=False):
        super().__init__("mp3", ".mp3", quality, binary)
        self._comparg = "-b:a"
        if not cbr:
            self.compvals = ["0", "1", "2", "3", "4", "5", "6", "7", "8"]
            self._comparg = "-q:a"
        self._cbr = cbr

    def getargs(self, ifile, ofile):
        return [
            self.binary,
            "-hide_banner",
            "-v",
            "error",
            "-i",
            ifile,
            "-c:a",
            "libmp3lame",
            self._comparg,
            self.compval,
            "-y",
            ofile,
        ]

    def __str__(self):
        return super(MP3, self).__str__() + f", cbr: {self._cbr} "


class OPUS(LossyCodec):
    def __init__(self, quality=3, binary=None):
        super().__init__(
            "opus",
            ".opus",
            quality,
            binary,
            compvals=[
                "224k",
                "192k",
                "160k",
                "144k",
                "128k",
                "112k",
                "96k",
                "80k",
                "64k",
            ],
        )

    def getargs(self, ifile, ofile):
        return [
            self.binary,
            "-hide_banner",
            "-v",
            "error",
            "-i",
            ifile,
            "-c:a",
            "libopus",
            "-b:a",
            self.compval,
            "-vn",
            "-y",
            ofile,
        ]


class Vorbis(LossyCodec):
    def __init__(self, quality=2, binary=None):
        super().__init__(
            "vorbis",
            ".ogg",
            quality,
            binary,
            compvals=["7", "6", "5", "4.5", "4", "3", "2", "1", "0"],
        )

    # -b or -b:a does not work for libvorbis.
    # The documentation is incorrect: https://ffmpeg.org/ffmpeg-codecs.html#libvorbis
    def getargs(self, ifile, ofile):
        return [
            self.binary,
            "-hide_banner",
            "-v",
            "error",
            "-i",
            ifile,
            "-c:a",
            "libvorbis",
            "-q:a",
            self.compval,
            "-vn",
            "-y",
            ofile,
        ]


class WAV(LosslessCodec):
    def __init__(self, binary=None):
        super().__init__("wav", ".wav", binary)

    def getargs(self, ifile, ofile):
        return [self.binary, "-hide_banner", "-v", "error", "-i", ifile, "-y", ofile]


def try_with_lock(func):
    def _try_with_lock(self, *args, **kwargs):
        with type(self)._lock:
            try:
                func(self, *args, **kwargs)
            except Exception as e:
                log.e(str(e))
                self.errors += 1

    return _try_with_lock


class Converter(threading.Thread):
    _threadcount = 0
    _lock = threading.RLock()

    def __init__(
        self,
        files,
        codec,
        odir=None,
        removeoriginal=False,
        overwrite=False,
        convertlossy=False,
        copy=None,
        binary="ffprobe",
    ):
        """
        Creates a new Converter object. 

        This class can be used to batch convert lossless audio files using 
        FFmpeg.  

        If ifile is a file in files:

        It converts ifile to self.odir if it is:
        - a lossless audio file;
        - a lossy audio file and self.convertlossy is True.

        It copies ifile to self.odir if it is:
        - an image and self.copy is 'images';
        - a non-audio file and self.copy is 'all'. 

        :param files: An iterable of files.
        :param copy: The non-audio files to copy: 'all', 'images', or None. 
        :param str odir: The output directory.
        :param bool removeoriginal: If True, the original audio files will be 
                                    deleted after conversion.
        :param bool overwrite: If True, ifile will replace any file with the same name.
        :param bool convertlossy: If True, ifile will be encoded even if it is a lossy 
                                  audio file.
        :param str binary: The ffprobe binary (only needed if converting from MPEG-4).
        :returns: True if successful, otherwise False.
        """
        threading.Thread.__init__(self)
        type(self)._threadcount += 1
        self.id = type(self)._threadcount
        self.errors = 0
        self.files = files
        self.codec = codec
        if not odir:
            odir = DEFAULTPATH
        self.odir = odir
        self.removeoriginal = removeoriginal
        self.overwrite = overwrite
        self.convertlossy = convertlossy
        if not (copy == "all" or copy == "images" or copy == None):
            raise ValueError(
                f'{copy} is invalid for copy. Valid values are: "all", "images", and None.'
            )
        self.copy = copy
        self.binary = binary

    def run(self):
        while True:
            with type(self)._lock:
                try:
                    ifile = os.path.normpath(next(self.files))
                except StopIteration:
                    return
            lower = ifile.lower()
            # Check for MPEG-4 container first because it can contain lossy or lossless audio.
            if lower.endswith(".m4a") and self.get_audio_codec(ifile) == "alac":
                self.ffmpegconvert(ifile)

            elif lower.endswith((".flac", ".wav")):
                self.ffmpegconvert(ifile)

            elif self.convertlossy and lower.endswith(
                (".m4a", ".mp3", ".ogg", ".opus")
            ):
                self.ffmpegconvert(ifile)

            elif self.copy == "all" or (
                self.copy == "images" and lower.endswith((".jpg", "jpeg", "png"))
            ):
                self.copyfile(ifile)

    @try_with_lock
    def copyfile(self, ifile):
        """Copies ifile to the output directory self.odir. 

        :param file ifile: The file to copy. 
        """
        ofile = os.path.join(self.odir, ifile)
        self.makedir(ofile)
        if not self.overwrite:
            if os.path.exists(ofile):
                log.i(
                    f'Thread {self.id} did not copy "{ifile}" to '
                    f'"{ofile}" because it already exists.'
                )
                return
        shutil.copy2(ifile, ofile)
        log.i(f'Copied "{ifile}" to "{ofile}"')

    @try_with_lock
    def makedir(self, ofile):
        """Makes a directory for ofile if it does not exist. 

        :param file ofile: The output file.
        """
        odir = os.path.dirname(ofile)
        if not os.path.exists(odir):
            os.makedirs(odir)

    def get_audio_codec(self, ifile):
        """
        Checks the audio encoding inside the MPEG-4 container. 
        ValueError is raised if ifile is not an MPEG-4 container.

        :param file ifile: The audio or video file.
        :returns str codec: The audio codec of ifile.
        """
        p = Popen(
            [
                self.binary,
                "-v",
                "error",
                "-select_streams",
                "a:0",
                "-show_entries",
                "stream=codec_name",
                "-of",
                "default=noprint_wrappers=1:nokey=1",
                ifile,
            ],
            stdout=PIPE,
            stderr=PIPE,
            encoding="utf-8",
        )
        return p.communicate()[0].strip()

    def ffmpegconvert(self, ifile):
        """Converts ifile using FFmpeg. 
        :param file ifile: The lossless audio file to encode.
        :returns: True if conversion is successful, otherwise False.
        """
        swap_ext = os.path.splitext(ifile)[0] + self.codec.ext
        ofile = os.path.join(self.odir, swap_ext)
        self.makedir(ofile)
        if not self.overwrite:
            if os.path.exists(ofile):
                log.d(
                    f'Thread {self.id} did not convert "{ifile}" to "{ofile}" '
                    "because it already exists."
                )
                return
        result = self.codec.encode(ifile, ofile)
        if result["sterr"]:
            log.e(f"{ifile}: " + result["sterr"])
            self.errors += 1
        else:
            log.i(f'Converted "{ifile}" to "{self.odir}" in thread {self.id}')
        if self.removeoriginal:
            try:
                os.remove(ifile)
                log.i(f'Deleted "{ifile}" in thread {self.id}')
            except IOError:
                log.e(f'Failed to delete "{ifile}"')
                self.errors += 1


def run_converters(converters):
    errors = 0
    for converter in converters:
        converter.start()
        log.d(f"Started thread {converter.id}")
        time.sleep(0.01)
    for converter in converters:
        converter.join()
        errors += converter.errors
        log.d(f"Joined thread {converter.id}")
    return errors


def batchconvert(
    files,
    codec,
    procnum=4,
    odir=None,
    removeoriginal=False,
    overwrite=False,
    convertlossy=False,
    copy=None,
):
    """Batch encodes audio files using FFmpeg.

    If ifile is a file in files:

    :param files: An iterable of files.
    :param copy: The non-audio files to copy: 'all', 'images', or None. 
    :param str odir: The output directory.
    :param bool removeoriginal: If True, the original audio files will be deleted 
                                after conversion.
    :param bool overwrite: If True, ifile will replace any file with the same name.
    :param bool convertlossy: If True, ifile will be encoded even if it is a lossy 
                              audio file.
    :returns dict result: A dictionary containing the error count and the 
                          runtime (in seconds).
    """
    t1 = timeit.default_timer()
    result = {"errors": 0, "runtime": 0}
    converters = [
        Converter(files, codec, odir, removeoriginal, overwrite, convertlossy, copy)
        for _ in range(procnum)
    ]
    result["errors"] = run_converters(converters)
    t2 = timeit.default_timer()
    rt = result["runtime"] = t2 - t1
    log.i(f"Runtime: {rt:.2f} seconds")
    return result
