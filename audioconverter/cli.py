#!/usr/bin/env python3

import configparser
import os
import shutil
import sys

from audioconverter.core import DEFAULTPATH, LossyCodec, batchconvert, codecfactory
from audioconverter.log import LOGPATH

if __name__ == "__main__":
    dn = os.path.dirname
    rootmodule = os.path.join(dn(dn(os.path.realpath(__file__))))
    sys.path.append(rootmodule)


def initini(path):
    """
    Initialises the ini configuration file.

    Generates an ini configuration file in the user's home directory if it does 
    not exist. Otherwise, it reads the file and returns a ConfigParser object 
    based on its contents.

    :returns ConfigParser ini: The user settings contained within the ini file.
    """
    ini = configparser.ConfigParser(
        allow_no_value=True, inline_comment_prefixes=("#", ";")
    )
    ini_exists = True
    if not os.path.isfile(path):
        ini_exists = False
        ini.add_section("default")
        ini.set(
            "default",
            "codec",
            "opus # aac, alac, flac, mp3, mp3_cbr, opus, vorbis or wav.",
        )
        ini.set(
            "default",
            "number_of_processes",
            "5 # The number of parallel FFmpeg proccesses.",
        )
        ini.set(
            "default",
            "# The location of the FFmpeg binary. This can be empty if FFmpeg is added to your system's path variable.",
        )
        ini.set("default", "binary")
        ini.set("default", "output_folder", DEFAULTPATH)
        ini.set(
            "default",
            "remove_original",
            "false # Input files will be deleted if this is true.",
        )
        ini.set("default", "overwrite_existing", "false")
        ini.set("default", "convert_lossy", "false")
        ini.set("default", "# Copy over all files, only images, or none.")
        ini.set("default", '# Enter "all", "images" or leave it empty.')
        ini.set(
            "default",
            "copy",
            "all # Would you like to copy over [all] files into the output folder, only [images], or just the converted audio--leave this empty if so.",
        )
        ini.add_section("quality")
        ini.set("quality", "# 0 is the highest audio quality and 8 is the lowest.")
        ini.set("quality", "aac", "3")
        ini.set("quality", "mp3", "2")
        ini.set("quality", "opus", "3")
        ini.set("quality", "vorbis", "3")
        ini.add_section("extensions")
        ini.set("extensions", "flac", ".flac")
        ini.set("extensions", "vorbis", ".ogg")
        ini.set(
            "extensions",
            "opus",
            ".ogg # It is written in the spec to use .opus, but there is greater support for .ogg e.g. Win 10 (1709+) and Android 6+.",
        )
        try:
            with open(path, "w") as x:
                ini.write(x)
        except IOError as e:
            sys.exit(e)
    if not ini_exists:
        sys.exit(
            f"An .ini configuration file has been added to {path}. "
            "Please edit this file with your preferences and then "
            "start AudioConverter from the command line again."
        )
    ini.read(path)
    return ini


def warndeleting():
    """Warns the user if they are about to delete the original audio files."""
    while True:
        decision = input(
            "Are you sure you want to delete the original files? " "(Y/n): "
        )
        if decision.lower() == "y" or not decision:
            return
        elif decision.lower() == "n":
            sys.exit(0)


def codec(ini):
    codec = None
    try:
        choice = ini["default"]["codec"].lower()
        codec = codecfactory(choice)
        if issubclass(type(codec), LossyCodec):
            codec.quality = int(ini["quality"][choice])
    except Exception as e:
        sys.exit(e)

    try:
        binary = ini["default"]["binary"]
        if binary:
            codec.binary = binary
    except Exception:
        sys.exit("Check binary path in audioconverter.ini")

    try:
        ext = ini["extensions"][codec.name]
        codec.ext = ext
    except Exception:
        pass

    return codec


def procnum(ini):
    try:
        return int(ini["default"]["number_of_processes"])
    except Exception:
        sys.exit("Check the number of processes in audioconverter.ini")


def odir(ini):
    try:
        return ini["default"]["output_folder"]
    except Exception:
        return DEFAULTPATH


def removeoriginal(ini):
    try:
        return ini["default"]["remove_original"].lower() == "true"
    except Exception:
        sys.exit('Check remove_original is "true" or "false" in ' "audioconverter.ini")


def convertlossy(ini):
    try:
        return ini["default"]["convert_lossy"].lower() == "true"
    except Exception:
        sys.exit('Check convert_lossy is "true" or "false" in ' "audioconverter.ini")


def copy(ini):
    try:
        choice = ini["default"]["copy"].lower()
        if not choice:
            return None
        elif choice == "all" or choice == "images":
            return choice
        raise ValueError
    except Exception:
        sys.exit(
            'Check copy_other is "all", "images" or empty in ' "audioconverter.ini"
        )


def overwrite(ini):
    try:
        return ini["default"]["overwrite_existing"].lower() == "true"
    except Exception:
        sys.exit(
            'Check overwrite_existing is "true" or "false" in ' "audioconverter.ini"
        )


def walkdir(num=-1):
    """
    Walks the directory recursively and yields files, one at a time.

    :param int num: The number of files to yield. If num is a negative value,
                    then there is no limit.    
    """
    for path, _, files in os.walk("."):
        for f in files:
            if num == 0:
                return
            yield os.path.join(path, f)
            num -= 1


def print_conversion_config(userconf):
    print("Audio Converter\n" "---------------")
    for k, v in userconf.items():
        print(f"{k} = {v}")
    print("Your audio is being converted, please wait...")


def formatruntime(runtime):
    runtime = int(runtime)
    unit = "seconds"
    if runtime > 60:
        runtime = int(runtime / 60)
        unit = "minutes"
    return f"{runtime:.0f} {unit}"


def print_conversion_result(errors):
    if errors == 0:
        print("Conversion complete!")
    else:
        sys.exit(
            f"There were {errors} errors during the conversion. Please "
            f'check the logfile at "{LOGPATH}" for more '
            "information."
        )

def main():
    # Get the user's settings from the INI file.
    ini = initini(os.path.join(os.path.expanduser("~"), ".audioconverter.ini"))
    userconf = {
        "codec": codec(ini),
        "procnum": procnum(ini),
        "odir": odir(ini),
        "removeoriginal": removeoriginal(ini),
        "overwrite": overwrite(ini),
        "convertlossy": convertlossy(ini),
        "copy": copy(ini),
    }
    if userconf["removeoriginal"] == True:
        warndeleting()
    files = walkdir()
    print_conversion_config(userconf)
    result = batchconvert(files, **userconf)
    print_conversion_result(result["errors"])
    runtime = formatruntime(result["runtime"])
    print(f"Runtime: {runtime}")


if __name__ == "__main__":
    main()
