import datetime
import logging
import os

# TODO: make LOGPATH modifiable
LOGPATH = os.path.join(os.path.expanduser("~"), '.audioconverter.log')
logging.basicConfig(
    handlers=[logging.FileHandler(LOGPATH, 'w', 'utf-8')],
    level=logging.ERROR
)


def _prependtime(msg):
    dt = str(datetime.datetime.now())[:-7]
    return f'{dt} {msg}'


def i(msg):
    return logging.info(_prependtime(msg))


def d(msg):
    return logging.debug(_prependtime(msg))


def w(msg):
    return logging.warning(_prependtime(msg))


def e(msg):
    return logging.error(_prependtime(msg))
