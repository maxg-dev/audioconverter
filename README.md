# AudioConverter

AudioConverter is a batch audio converter powered by 
[FFmpeg](https://ffmpeg.org/). It (recursively) converts lossless audio files 
within a folder using multiple FFmpeg processes.

## Supported codecs
+ AAC
+ ALAC
+ FLAC
+ MP3
+ Opus
+ Vorbis
+ WAV

## Requirements
+ [FFmpeg](https://ffmpeg.org/)

## Installation

#### Linux
```
python3 -m pip install --user . 
```

#### Windows
```
py -m pip install --user .
```

## Setup
1. In a terminal enter `audiocon` and hit enter.
2. Modify the INI file that is created with your conversion preferences.

## Usage
In a terminal run `audiocon` in the source music directory.

## License

Copyright &copy; 2019 [Max](https://www.maxg.dev)

[![GNU GPLv3 logo][GPLv3-logo]](https://www.gnu.org/licenses/gpl-3.0.en.html)

[GPLv3-logo]: https://www.gnu.org/graphics/gplv3-127x51.png
