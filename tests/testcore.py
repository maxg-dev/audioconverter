import unittest
from audioconverter.core import (
    AAC,
    ALAC,
    CompType,
    FLAC,
    MP3,
    OPUS,
    Vorbis,
    WAV,
)

DEFAULTBITRATES = ['320k', '256k', '224k', '192k',
                   '160k', '128k', '112k', '96k', '80k']


class TestCodecs(unittest.TestCase):

    def setUp(self):
        self.aac = AAC()
        self.alac = ALAC()
        self.flac = FLAC()
        self.mp3 = MP3()
        self.mp3_cbr = MP3(cbr=True)
        self.opus = OPUS()
        self.vorbis = Vorbis()
        self.wav = WAV()
        self.codecs = (self.aac, self.alac, self.flac, self.mp3,
                       self.mp3_cbr, self.opus, self.vorbis, self.wav)
        self.lossycodecs = [x for x in self.codecs
                            if x.comptype == CompType.LOSSY]
        self.losslesscodecs = [x for x in self.codecs
                               if x.comptype == CompType.LOSSLESS]

    def test_compvals(self):
        self.assertEqual(self.aac.compvals, DEFAULTBITRATES)
        self.assertEqual(self.mp3.compvals,
                         ['0', '1', '2', '3', '4', '5', '6', '7', '8'])
        self.assertEqual(self.mp3_cbr.compvals, DEFAULTBITRATES)
        self.assertEqual(self.opus.compvals, DEFAULTBITRATES)
        self.assertEqual(self.vorbis.compvals,
                         ['7', '6', '5', '4', '3', '2', '1', '0', '0'])

    def test_comptype(self):
        self.assertEqual(self.aac.comptype, CompType.LOSSY)
        self.assertEqual(self.alac.comptype, CompType.LOSSLESS)
        self.assertEqual(self.flac.comptype, CompType.LOSSLESS)
        self.assertEqual(self.mp3.comptype, CompType.LOSSY)
        self.assertEqual(self.opus.comptype, CompType.LOSSY)
        self.assertEqual(self.vorbis.comptype, CompType.LOSSY)
        self.assertEqual(self.wav.comptype, CompType.LOSSLESS)

    def test_extension(self):
        self.assertEqual(self.aac.ext, '.m4a')
        self.assertEqual(self.alac.ext, '.m4a')
        self.assertEqual(self.flac.ext, '.flac')
        self.assertEqual(self.mp3.ext, '.mp3')
        self.assertEqual(self.opus.ext, '.opus')
        self.assertEqual(self.vorbis.ext, '.ogg')
        self.assertEqual(self.wav.ext, '.wav')

    def test_quality(self):
        self.assertEqual(self.aac.quality, 2)
        self.assertEqual(self.mp3.quality, 1)
        self.assertEqual(self.opus.quality, 3)
        self.assertEqual(self.vorbis.quality, 2)

        for codec in self.lossycodecs:
            codec.quality = 8
            self.assertEqual(codec.quality, 8)
            codec.quality = 0
            self.assertEqual(codec.quality, 0)
            codec.quality = '0'
            self.assertEqual(codec.quality, 0)

    def test_args(self):
        for codec in self.codecs:
            for arg in codec.getargs('in', 'out'):
                self.assertEqual(type(arg), str)


if __name__ == '__main__':
    unittest.main()
